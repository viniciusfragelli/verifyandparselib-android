package br.com.virtualtechnology.verifyandparse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by vinic on 14/10/2017.
 */

public class Parse {

    public static String toJSON(Object ob){
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        return builder.create().toJson(ob);
    }

    public static Object fromJSON(String json, Class klass){
        Gson gson = new Gson();
        return gson.fromJson(json,klass);
    }

    public static Object fromJSON(String json, Type type){
        Gson gson = new Gson();
        return gson.fromJson(json,type);
    }

    public static <T> Object fromJSON(String json){
        Gson gson = new Gson();
        return gson.fromJson(json,new TypeToken<T>(){}.getType());
    }

    public static String formataData(java.sql.Date data){
        if(data == null)return "Sem registro";
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(new java.util.Date(data.getTime()));
    }

    public static String formataData(Object data, String format){
        if(data == null)return "Sem registro";
        SimpleDateFormat formatDate = new SimpleDateFormat(format);
        return formatDate.format(data);
    }

    public static String formataHora(java.sql.Time hora){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(new java.util.Date(hora.getTime()));
    }

    public static String formataValor(float valor){
        String s = Float.toString(valor);
        for(int i = 0;i<s.length();i++){
            if(s.charAt(i) == '.'){
                if(i+4 <= s.length()){
                    s = s.substring(0, i+3);
                    return s.replace('.', ',');
                }else{
                    if(i+3 <= s.length()){
                        return s.replace('.', ',');
                    }else{
                        if(i+2 <= s.length()){
                            return (s + "0").replace('.', ',');
                        }else{
                            if(i+1 <= s.length()){
                                return (s + "00").replace('.', ',');
                            }
                        }
                    }
                }
            }
        }
        return (s + ".00").replace('.', ',');
    }

    public static String formataTelefoneParaDiscagem(String s){
        return s.replace("(","").replace(")","").replace("-","").trim();
    }

    public static String ConverteMesIntToStringDe0A11(int k){
        String s = "";
        switch(k){
            case 0: s = "Janeiro"; break;
            case 1: s = "Fevereiro"; break;
            case 2: s = "Marco"; break;
            case 3: s = "Abril"; break;
            case 4: s = "Maio"; break;
            case 5: s = "Junho"; break;
            case 6: s = "Julho"; break;
            case 7: s = "Agosto"; break;
            case 8: s = "Setembro"; break;
            case 9: s = "Outubro"; break;
            case 10: s = "Novembro"; break;
            case 11: s = "Dezembro"; break;
            default: s = "error";
        }
        return s;
    }

    public static int converteMesStringToIntDe0A11(String s){
        int k = -1;
        switch(s){
            case "Janeiro": k = 0; break;
            case "Fevereiro": k = 1; break;
            case "Marco": k = 2; break;
            case "Abril": k = 3; break;
            case "Maio": k = 4; break;
            case "Junho": k = 5; break;
            case "Julho": k = 6; break;
            case "Agosto": k = 7; break;
            case "Setembro": k = 8; break;
            case "Outubro": k = 9; break;
            case "Novembro": k = 10; break;
            case "Dezembro": k = 11; break;
        }
        return k;
    }



    public static String stStringNullable(String s){
        if(s == null){
            return "NULL";
        }else{
            return "'"+s+"'";
        }
    }

    public static String stStringNullable(Integer s){
        if(s == null){
            return "NULL";
        }else{
            return "'"+s+"'";
        }
    }

    public static String stStringNullable(Float s){
        if(s == null){
            return "NULL";
        }else{
            return "'"+s.toString()+"'";
        }
    }

    public static String formataValor(Float valor, boolean isbank){
        if(isbank){
            return stStringNullable(valor);
        }else{
            if(valor == null)return "";
            return NumberFormat.getCurrencyInstance().format(valor).replace("R$ ", "");
        }
    }

    public static String formataValor(String valor, boolean isbank){
        if(isbank){
            return stStringNullable(valor);
        }else{
            if(valor == null)return "";
            Float f = new Float(valor.replace("'", ""));
            return NumberFormat.getCurrencyInstance().format(f).replace("R$ ", "");
        }
    }

    public static String formataInt(Integer valor, boolean isbank){
        if(isbank){
            return stStringNullable(valor);
        }else{
            if(valor == null)return "";
            return valor+"";
        }
    }

    public static String formataInt(Object valor, boolean isbank){
        if(isbank){
            if(valor == null){
                return "NULL";
            }else{
                return stStringNullable(Integer.parseInt(valor.toString()));
            }
        }else{
            if(valor == null)return "";
            return valor+"";
        }
    }

    public static Integer formataIntParse(Object valor, boolean isbank){
        if(valor != null){
            if(valor.toString().equalsIgnoreCase("")){
                return null;
            }else{
                return Integer.parseInt(valor.toString());
            }
        }else{
            return null;
        }
    }

    public static String formataValor(Object valor, boolean isbank){
        if(isbank){
            if(valor != null){
                return stStringNullable(Float.parseFloat(valor.toString()));
            }else{
                return "NULL";
            }
        }else{
            if(valor == null)return "";
            return NumberFormat.getCurrencyInstance().format(valor).replace("R$ ", "");
        }
    }

    public static int converteMesStringToIntDe1A12(String s){
        int k = -1;
        switch(s){
            case "Janeiro": k = 1; break;
            case "Fevereiro": k = 2; break;
            case "Marco": k = 3; break;
            case "Abril": k = 4; break;
            case "Maio": k = 5; break;
            case "Junho": k = 6; break;
            case "Julho": k = 7; break;
            case "Agosto": k = 8; break;
            case "Setembro": k = 9; break;
            case "Outubro": k = 10; break;
            case "Novembro": k = 11; break;
            case "Dezembro": k = 12; break;
        }
        return k;
    }

    public static int convertLongEmMinutos(long tempototal){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        int dia = (Integer.parseInt(format.format(tempototal).substring(0,2)) - 1);
        int mes = (Integer.parseInt(format.format(tempototal).substring(3,5)) - 1);
        int ano = (Integer.parseInt(format.format(tempototal).substring(6,10)) - 1970);
        int hora = (Integer.parseInt(format.format(tempototal).substring(11,13)));
        int min = (Integer.parseInt(format.format(tempototal).substring(14)));
        min += hora*60;
        min += dia*24*60;
        min += mes*40320;
        min += ano*12*40320;
        return min;
    }

    public static String formataAno(int k){
        return "000"+k;
    }

    public static String formataData(int k) {
        switch (k) {
            case 0:
                return "00";
            case 1:
                return "01";
            case 2:
                return "02";
            case 3:
                return "03";
            case 4:
                return "04";
            case 5:
                return "05";
            case 6:
                return "06";
            case 7:
                return "07";
            case 8:
                return "08";
            case 9:
                return "09";
            default:
                return k + "";
        }
    }



    public static String formataData(Date data, boolean isbank){
        if(data == null){
            return null;
        }
        if(isbank){
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            return stStringNullable(format.format(data));
        }else{
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            return format.format(data);
        }
    }

    public static String formataTextoOut(String s,boolean isbank){
        if(isbank){
            if(s == null){
                return null;
            }else{
                return "'"+s.replace("'", "")+"'";
            }
        }else{
            if(s != null){
                return ""+s;
            }else{
                return "";
            }
        }
    }

    public static String formataTextoIn(String s){
        if(s == null){
            return null;
        }else{
            if(s.equalsIgnoreCase("")){
                return null;
            }else{
                return s;
            }
        }
    }

    public static String converte_DDMMYYYY_To_YYYYMMDD(String data){
        if(data.equalsIgnoreCase("") || data.equalsIgnoreCase("  /  /    "))return null;
        try {
            SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd");
            return yyyymmdd.format(ddmmyyyy.parse(data));
        } catch (ParseException ex) {
            ex.printStackTrace();
            return data;
        }
    }
}
