package br.com.virtualtechnology.verifyandparse;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vinicius on 25/11/2015.
 */
public class Verify {

    public static String getFistString(String u){
        String aux = "";
        for (int i = 0; i<u.length();i++){
            if(u.charAt(i) == '-'){
                break;
            }else{
                aux += u.charAt(i);
            }
        }
        return aux.trim();
    }

    public static String getSegString(String u){
        String aux = "";
        boolean teste = false;
        for (int i = 0; i<u.length();i++){
            if(u.charAt(i) == '-' && !teste){
                teste = true;
            }else{
                aux += u.charAt(i);
            }
        }
        return aux.trim();
    }

    public static String verificaELimpaCharEspeciaisUserSenha(String s){
        String get = s;
        s = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A': aux = aux+get.charAt(i); break;
                case 'B': aux = aux+get.charAt(i); break;
                case 'C': aux = aux+get.charAt(i); break;
                case 'D': aux = aux+get.charAt(i); break;
                case 'E': aux = aux+get.charAt(i); break;
                case 'F': aux = aux+get.charAt(i); break;
                case 'G': aux = aux+get.charAt(i); break;
                case 'H': aux = aux+get.charAt(i); break;
                case 'I': aux = aux+get.charAt(i); break;
                case 'J': aux = aux+get.charAt(i); break;
                case 'K': aux = aux+get.charAt(i); break;
                case 'L': aux = aux+get.charAt(i); break;
                case 'M': aux = aux+get.charAt(i); break;
                case 'N': aux = aux+get.charAt(i); break;
                case 'O': aux = aux+get.charAt(i); break;
                case 'P': aux = aux+get.charAt(i); break;
                case 'Q': aux = aux+get.charAt(i); break;
                case 'R': aux = aux+get.charAt(i); break;
                case 'S': aux = aux+get.charAt(i); break;
                case 'T': aux = aux+get.charAt(i); break;
                case 'U': aux = aux+get.charAt(i); break;
                case 'V': aux = aux+get.charAt(i); break;
                case 'W': aux = aux+get.charAt(i); break;
                case 'X': aux = aux+get.charAt(i); break;
                case 'Y': aux = aux+get.charAt(i); break;
                case 'Z': aux = aux+get.charAt(i); break;
                case '0': aux = aux+get.charAt(i); break;
                case '1': aux = aux+get.charAt(i); break;
                case '2': aux = aux+get.charAt(i); break;
                case '3': aux = aux+get.charAt(i); break;
                case '4': aux = aux+get.charAt(i); break;
                case '5': aux = aux+get.charAt(i); break;
                case '6': aux = aux+get.charAt(i); break;
                case '7': aux = aux+get.charAt(i); break;
                case '8': aux = aux+get.charAt(i); break;
                case '9': aux = aux+get.charAt(i); break;
                default: break;
            }
        }
        return aux;
    }

    public static String verificaELimpaCharEspeciaisNomeECPF(String s){
        String get = s;
        s = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A': aux = aux+get.charAt(i); break;
                case 'B': aux = aux+get.charAt(i); break;
                case 'C': aux = aux+get.charAt(i); break;
                case 'D': aux = aux+get.charAt(i); break;
                case 'E': aux = aux+get.charAt(i); break;
                case 'F': aux = aux+get.charAt(i); break;
                case 'G': aux = aux+get.charAt(i); break;
                case 'H': aux = aux+get.charAt(i); break;
                case 'I': aux = aux+get.charAt(i); break;
                case 'J': aux = aux+get.charAt(i); break;
                case 'K': aux = aux+get.charAt(i); break;
                case 'L': aux = aux+get.charAt(i); break;
                case 'M': aux = aux+get.charAt(i); break;
                case 'N': aux = aux+get.charAt(i); break;
                case 'O': aux = aux+get.charAt(i); break;
                case 'P': aux = aux+get.charAt(i); break;
                case 'Q': aux = aux+get.charAt(i); break;
                case 'R': aux = aux+get.charAt(i); break;
                case 'S': aux = aux+get.charAt(i); break;
                case 'T': aux = aux+get.charAt(i); break;
                case 'U': aux = aux+get.charAt(i); break;
                case 'V': aux = aux+get.charAt(i); break;
                case 'W': aux = aux+get.charAt(i); break;
                case 'X': aux = aux+get.charAt(i); break;
                case 'Y': aux = aux+get.charAt(i); break;
                case 'Z': aux = aux+get.charAt(i); break;
                case '0': aux = aux+get.charAt(i); break;
                case '1': aux = aux+get.charAt(i); break;
                case '2': aux = aux+get.charAt(i); break;
                case '3': aux = aux+get.charAt(i); break;
                case '4': aux = aux+get.charAt(i); break;
                case '5': aux = aux+get.charAt(i); break;
                case '6': aux = aux+get.charAt(i); break;
                case '7': aux = aux+get.charAt(i); break;
                case '8': aux = aux+get.charAt(i); break;
                case '9': aux = aux+get.charAt(i); break;
                case ' ': aux = aux+get.charAt(i); break;
                case '.': aux = aux+get.charAt(i); break;
                case '-': aux = aux+get.charAt(i); break;
                default: break;
            }
        }
        return aux;
    }

    public static boolean verificaIsNumero(String s){
        if(!s.equals("")){
            s = s.toUpperCase();
            for(int i = 0;i<s.length();i++){
                switch(s.charAt(i)){
                    case '0': break;
                    case '1': break;
                    case '2': break;
                    case '3': break;
                    case '4': break;
                    case '5': break;
                    case '6': break;
                    case '7': break;
                    case '8': break;
                    case '9': break;
                    default: return false;
                }
            }
            return true;
        }else{
            return false;
        }
    }

    public static boolean verificaSeECNPJouCPF(String cpf){
        if(cpf.equals(""))return false;
        if(cpf == null)return false;
        if(cpf.length() == 14){
            if(verificaIsNumero(cpf.substring(0,3))){
                if(cpf.charAt(3) == '.'){
                    if(verificaIsNumero(cpf.substring(4,7))){
                        if(cpf.charAt(7) == '.'){
                            if(verificaIsNumero(cpf.substring(8,11))){
                                if(cpf.charAt(11) == '-'){
                                    if(verificaIsNumero(cpf.substring(12))){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                }else{
                                    return false;
                                }
                            }else{
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            if(cpf.length() == 18){
                if(verificaIsNumero(cpf.substring(0,2))){
                    if(cpf.charAt(2) == '.'){
                        if(verificaIsNumero(cpf.substring(3,6))){
                            if(cpf.charAt(6) == '.'){
                                if(verificaIsNumero(cpf.substring(7,10))){
                                    if(cpf.charAt(10) == '/'){
                                        if(verificaIsNumero(cpf.substring(11,15))){
                                            if(cpf.charAt(15) == '-'){
                                                if(verificaIsNumero(cpf.substring(16))){
                                                    return true;
                                                }else{
                                                    return false;
                                                }
                                            }else{
                                                return false;
                                            }
                                        }else{
                                            return false;
                                        }
                                    }else{
                                        return false;
                                    }
                                }else{
                                    return false;
                                }
                            }else{
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }

    public static boolean verificaSeETelefone(String telefone){
        if(telefone.charAt(0) == '('){
            if(verificaIsNumero(telefone.substring(1, 3))){
                if(telefone.charAt(3) == ')'){
                    if(telefone.charAt(4) == ' '){
                        String aux = "";
                        int i;
                        for (i = 5; telefone.charAt(i) != '-' && i<telefone.length() ; i++) {
                            aux += telefone.charAt(i);
                        }
                        if(verificaIsNumero(aux)){
                            if(i < telefone.length()){
                                if(telefone.charAt(i) == '-'){
                                    aux = "";
                                    for (i = i + 1; i < telefone.length(); i++) {
                                        aux += telefone.charAt(i);
                                    }
                                    return verificaIsNumero(aux);
                                }else{
                                    return false;
                                }
                            }else{
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static String ConverteMesIntToStringDe1A12(int k){
        String s = "";
        switch(k){
            case 1: s = "Janeiro"; break;
            case 2: s = "Fevereiro"; break;
            case 3: s = "Marco"; break;
            case 4: s = "Abril"; break;
            case 5: s = "Maio"; break;
            case 6: s = "Junho"; break;
            case 7: s = "Julho"; break;
            case 8: s = "Agosto"; break;
            case 9: s = "Setembro"; break;
            case 10: s = "Outubro"; break;
            case 11: s = "Novembro"; break;
            case 12: s = "Dezembro"; break;
            default: s = "error";
        }
        return s;
    }

    public static boolean verificaSeEHoraValida(String hora){
        if(!hora.equals("")){
            if(hora.length() == 5){
                if(verificaIsNumero(hora.substring(0, 2))){
                    int h = Integer.parseInt(hora.substring(0, 2));
                    if(h >= 0 && h < 24){
                        if(hora.charAt(2) == ':'){
                            if(verificaIsNumero(hora.substring(3))){
                                int min = Integer.parseInt(hora.substring(3));
                                if(min >= 0 && min < 60){
                                    return true;
                                }else{
                                    return false;
                                }
                            }else{
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static Float formataValorEParseFloat(String s){
        if(s.equalsIgnoreCase(""))return null;
        if(s.length() > 3){
            s = s.substring(0, s.length()-3).replace(".", "")+s.substring(s.length()-3);
        }
        return Float.parseFloat(s.replace(",", "."));
    }

    public static boolean verificaIsValorComercialComVirgulaValido(String s){
        if(!s.equals("")){
            s = s.toUpperCase();
            if(s.length() > 3){
                s = s.substring(0, s.length()-3).replace(".", "")+s.substring(s.length()-3);
            }
            int cont = 0;
            for(int i = 0;i<s.length();i++){
                switch(s.charAt(i)){
                    case '0': break;
                    case '1': break;
                    case '2': break;
                    case '3': break;
                    case '4': break;
                    case '5': break;
                    case '6': break;
                    case '7': break;
                    case '8': break;
                    case '9': break;
                    case '.': cont++; break;
                    case ',': cont++; break;
                    default: return false;
                }
            }
            if(cont <= 1){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static boolean verificaIsDataValida(String data){
        if(data.length() == 10){
            if(verificaIsNumero(data.substring(0, 2)) && data.charAt(2) == '/' && verificaIsNumero(data.substring(3,5)) && data.charAt(5) == '/' && verificaIsNumero(data.substring(6))){
                if(Integer.parseInt(data.substring(0, 2)) <= 31 && Integer.parseInt(data.substring(3,5)) <= 12){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static String verificaELimpaCharEspeciaisNome(String s){
        if(s.equals(""))return "";
        s = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A': aux = aux+s.charAt(i); break;
                case 'B': aux = aux+s.charAt(i); break;
                case 'C': aux = aux+s.charAt(i); break;
                case 'D': aux = aux+s.charAt(i); break;
                case 'E': aux = aux+s.charAt(i); break;
                case 'F': aux = aux+s.charAt(i); break;
                case 'G': aux = aux+s.charAt(i); break;
                case 'H': aux = aux+s.charAt(i); break;
                case 'I': aux = aux+s.charAt(i); break;
                case 'J': aux = aux+s.charAt(i); break;
                case 'K': aux = aux+s.charAt(i); break;
                case 'L': aux = aux+s.charAt(i); break;
                case 'M': aux = aux+s.charAt(i); break;
                case 'N': aux = aux+s.charAt(i); break;
                case 'O': aux = aux+s.charAt(i); break;
                case 'P': aux = aux+s.charAt(i); break;
                case 'Q': aux = aux+s.charAt(i); break;
                case 'R': aux = aux+s.charAt(i); break;
                case 'S': aux = aux+s.charAt(i); break;
                case 'T': aux = aux+s.charAt(i); break;
                case 'U': aux = aux+s.charAt(i); break;
                case 'V': aux = aux+s.charAt(i); break;
                case 'W': aux = aux+s.charAt(i); break;
                case 'X': aux = aux+s.charAt(i); break;
                case 'Y': aux = aux+s.charAt(i); break;
                case 'Z': aux = aux+s.charAt(i); break;
                case '0': aux = aux+s.charAt(i); break;
                case '1': aux = aux+s.charAt(i); break;
                case '2': aux = aux+s.charAt(i); break;
                case '3': aux = aux+s.charAt(i); break;
                case '4': aux = aux+s.charAt(i); break;
                case '5': aux = aux+s.charAt(i); break;
                case '6': aux = aux+s.charAt(i); break;
                case '7': aux = aux+s.charAt(i); break;
                case '8': aux = aux+s.charAt(i); break;
                case '9': aux = aux+s.charAt(i); break;
                case ' ': aux = aux+s.charAt(i); break;
                case '-': aux = aux+s.charAt(i); break;
                case 'Ã': aux = aux+'A'; break;
                case 'Á': aux = aux+'A'; break;
                case 'À': aux = aux+'A'; break;
                case 'Â': aux = aux+'A'; break;
                case 'Ä': aux = aux+'A'; break;
                case 'É': aux = aux+'E'; break;
                case 'Ê': aux = aux+'E'; break;
                case 'È': aux = aux+'E'; break;
                case 'Ë': aux = aux+'E'; break;
                case 'Í': aux = aux+'I'; break;
                case 'Ì': aux = aux+'I'; break;
                case 'Î': aux = aux+'I'; break;
                case 'Ï': aux = aux+'I'; break;
                case 'Õ': aux = aux+'O'; break;
                case 'Ó': aux = aux+'O'; break;
                case 'Ò': aux = aux+'O'; break;
                case 'Ô': aux = aux+'O'; break;
                case 'Ö': aux = aux+'O'; break;
                case 'Ú': aux = aux+'U'; break;
                case 'Ù': aux = aux+'U'; break;
                case 'Û': aux = aux+'U'; break;
                case 'Ü': aux = aux+'U'; break;
                case 'Ç': aux = aux+'C'; break;
                case 'Ñ': aux = aux+'N'; break;
                default: break;
            }
        }
        return aux;
    }

    public static boolean verificaSeECep(String cep){
        if(cep == null){
            return false;
        }else{
            if(!cep.equals("")){
                if(!cep.equals("     -   ")){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }

    public static boolean verificaSeELetrasSemEspaco(String s){
        if(s.equals(""))return false;
        s = s.toUpperCase();
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A':  break;
                case 'B':  break;
                case 'C':  break;
                case 'D':  break;
                case 'E':  break;
                case 'F':  break;
                case 'G':  break;
                case 'H':  break;
                case 'I':  break;
                case 'J':  break;
                case 'K':  break;
                case 'L':  break;
                case 'M':  break;
                case 'N':  break;
                case 'O':  break;
                case 'P':  break;
                case 'Q':  break;
                case 'R':  break;
                case 'S':  break;
                case 'T':  break;
                case 'U':  break;
                case 'V':  break;
                case 'W':  break;
                case 'X':  break;
                case 'Y':  break;
                case 'Z':  break;
                default: return false;
            }
        }
        return true;
    }

    public static boolean verificaSeELetrasComEspaco(String s){
        if(s.equals(""))return false;
        s = s.toUpperCase();
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A':  break;
                case 'B':  break;
                case 'C':  break;
                case 'D':  break;
                case 'E':  break;
                case 'F':  break;
                case 'G':  break;
                case 'H':  break;
                case 'I':  break;
                case 'J':  break;
                case 'K':  break;
                case 'L':  break;
                case 'M':  break;
                case 'N':  break;
                case 'O':  break;
                case 'P':  break;
                case 'Q':  break;
                case 'R':  break;
                case 'S':  break;
                case 'T':  break;
                case 'U':  break;
                case 'V':  break;
                case 'W':  break;
                case 'X':  break;
                case 'Y':  break;
                case 'Z':  break;
                default: return false;
            }
        }
        return true;
    }
}
